package kpi.com.kpishedule;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.thomashaertel.widget.MultiSpinner;

public class AddLongEventActivity extends Activity {

    private MultiSpinner spinner;
    private ArrayAdapter<String> adapter;
    String[] days = new String[]{"Mon", "Thu", "Wed", "Thu", "Fri", "Sat", "Sun"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_long_event_activity);

        View timeSelectionContainer = findViewById(R.id.time_selection_container);
        TextView first = (TextView) timeSelectionContainer.findViewById(R.id.first_input_text);
        TextView second = (TextView) timeSelectionContainer.findViewById(R.id.second_input_text);

        first.setText("час початку");
        second.setText("час кінця");

        final View dateRepeatSelectionContainer = findViewById(R.id.repeat_container);
        CheckBox repeatCheckbox = (CheckBox) findViewById(R.id.repeat_checkbox);
        repeatCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dateRepeatSelectionContainer.setVisibility(isChecked? View.VISIBLE: View.INVISIBLE);
            }
        });

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        for(String day: days){
            adapter.add(day);
        }
        // get spinner and set adapter
        spinner = (MultiSpinner) findViewById(R.id.spinnerMulti);
        spinner.setAdapter(adapter, false, onSelectedListener);

        // set initial selection
        boolean[] selectedItems = new boolean[adapter.getCount()];
        selectedItems[1] = true; // select second item
        setSpinnerSelection(spinner, selectedItems);
    }

    void setSpinnerSelection(MultiSpinner spinner, boolean[] selectedItems){
        spinner.setSelected(selectedItems);
    }

    private MultiSpinner.MultiSpinnerListener onSelectedListener = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            String selectedDays = "";
            for(int i = 0; i < selected.length; i ++){
                if(selected[i]){
                    selectedDays = selectedDays + days[i] + ", ";
                }
            }
            Toast.makeText(AddLongEventActivity.this, "selected:" + selectedDays, Toast.LENGTH_LONG).show();
        }
    };
}
