package kpi.com.kpishedule.d;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import kpi.com.kpishedule.data.Day;
import kpi.com.kpishedule.data.LessonEntry;
import kpi.com.kpishedule.data.Week;

public class FpmParser {

    public Week parse(String htmlString) {
        Document doc = Jsoup.parse(htmlString);
        Element table = doc.getElementsByClass("schedulTable").first();

        // rows in the table, from 1..6 - all lessons from Monday till Saturday
        // ignoring 0
        Week week = new Week();
        Element tbody = table.children().first();
        Elements rows = tbody.children();
        for (int i = 1; i < 7; i++) {
            Element row = rows.get(i);
            Elements tdElements = row.children();
            for (int j = 1; j < 7; j++) {
                // j for days Mon..Sat
                Day day = week.days.get(j - 1);
                Element tdE = tdElements.get(j);
                if (tdE.children().isEmpty()) {
                    day.addLesson(new LessonEntry(i), i - 1);
                } else {
                    Elements lessons = tdE.children().first().children().first().children();
                    String firstLessonString, secondLessonString;
                    firstLessonString = secondLessonString = null;
                    if(lessons.size() == 1){
                        Element tr = lessons.first().children().first();
                        if(tr.children().size() != 0){
                            firstLessonString = lessonByTr(tr);
                        }
                    } else if (lessons.size() == 2){
                        Element tr1 = lessons.first().children().first();
                        if(tr1.children().size() != 0){
                            secondLessonString = lessonByTr(tr1);
                        }
                        Element tr2 = lessons.last().children().first();
                        if(tr2.children().size() != 0){
                            firstLessonString = lessonByTr(tr2);
                        }
                    }
                    day.addLesson(new LessonEntry(i, firstLessonString, secondLessonString), i - 1);
                }

            }
        }

        return week;
    }

    String lessonByTr(Element trE){
        Elements aTags = trE.getElementsByTag("a");
        Elements nobr = trE.getElementsByTag("nobr");
        return aTags.first().text() + "\n" + aTags.get(1).text() + "\n" +nobr.text();
    }
}
