package kpi.com.kpishedule;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class AddEventActivity extends AppCompatActivity {

    Calendar startTime = Calendar.getInstance();
    Calendar endTime = Calendar.getInstance();
    EditText descriptionInput;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_event_activity);

        descriptionInput = (EditText) findViewById(R.id.description);
        initContainer((ViewGroup) findViewById(R.id.start_day_container), startTime);
        initContainer((ViewGroup) findViewById(R.id.end_day_container), endTime);

        findViewById(R.id.add_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addEvent();
            }
        });
    }

    void initContainer(ViewGroup container, Calendar time){
        View btn = container.findViewById(R.id.add_date_btn);
        EditText text = (EditText) container.findViewById(R.id.date_text);
        add(btn, text, time);
    }

    void add(View button, final EditText text, final Calendar time){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                        time.set(Calendar.YEAR, year);
                        time.set(Calendar.MONTH, month);
                        time.set(Calendar.DAY_OF_MONTH, day);
                        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
                                time.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                time.set(Calendar.MINUTE, minute);

                                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                text.setText(format.format(time.getTime()));
                            }
                        }, time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), false, true);
                        timePickerDialog.show(getSupportFragmentManager(), "");
                    }
                }, time.get(Calendar.YEAR), time.get(Calendar.MONTH), time.get(Calendar.DAY_OF_MONTH), true);
                datePickerDialog.show(getSupportFragmentManager(), "");
            }
        });
    }

    void addEvent(){
        Intent data = new Intent();
        data.putExtra(MainActivity.START_TIME, startTime);
        data.putExtra(MainActivity.END_TIME, endTime);
        data.putExtra(MainActivity.DESCRIPTION, descriptionInput.getText().toString());
        setResult(RESULT_OK, data);
        finish();
    }

    void save(Calendar c){

    }

}
