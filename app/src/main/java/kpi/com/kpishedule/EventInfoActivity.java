package kpi.com.kpishedule;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Artem Leus
 * on 1/31/16.
 */
public class EventInfoActivity extends AppCompatActivity {

    public static final String DESCRIPTION = "description";
    public static final String START_TIME = "start_time";
    public static final String END_TIME = "end_time";
    public static final String ID = "id";

    TextView startTime, endTime;
    EditText description;
    View saveBtn, editStartTimeBtn, editEndTimeBtn;
    long eventId;

    Calendar startTimeCalendar = Calendar.getInstance();
    Calendar endTimeCalendar = Calendar.getInstance();

    public static final int ACTION_EDIT = 1;
    public static final int ACTION_DELETE = 2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_info_activity);

        description = (EditText) findViewById(R.id.description);
        startTime = (TextView) findViewById(R.id.start_time);
        endTime = (TextView) findViewById(R.id.end_time);
        saveBtn = findViewById(R.id.save_btn);

        eventId = getIntent().getLongExtra(ID, -1);
        String startTimeString = getIntent().getStringExtra(START_TIME);
        String endTimeString = getIntent().getStringExtra(END_TIME);
        final String descriptionString = getIntent().getStringExtra(DESCRIPTION);

        description.setText(descriptionString);
        startTime.setText(startTimeString);
        endTime.setText(endTimeString);

        saveBtn.setVisibility(View.GONE);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });

        editStartTimeBtn = findViewById(R.id.edit_start_time);
        editEndTimeBtn = findViewById(R.id.edit_end_time);

        try {
            startTimeCalendar.setTime(Constants.DATE_FORMAT.parse(startTimeString));
            add(editStartTimeBtn, startTime, startTimeCalendar);

            endTimeCalendar.setTime(Constants.DATE_FORMAT.parse(endTimeString));
            add(editEndTimeBtn, endTime, endTimeCalendar);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        description.clearFocus();
        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!descriptionString.equals(s)){
                    saveBtn.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    void add(View button, final TextView text, final Calendar time){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
                        time.set(Calendar.YEAR, year);
                        time.set(Calendar.MONTH, month);
                        time.set(Calendar.DAY_OF_MONTH, day);
                        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
                                time.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                time.set(Calendar.MINUTE, minute);

                                text.setText(Constants.DATE_FORMAT.format(time.getTime()));
                                saveBtn.setVisibility(View.VISIBLE);
                            }
                        }, time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), false, true);
                        timePickerDialog.show(getSupportFragmentManager(), "");
                    }
                }, time.get(Calendar.YEAR), time.get(Calendar.MONTH), time.get(Calendar.DAY_OF_MONTH), true);
                datePickerDialog.show(getSupportFragmentManager(), "");
            }
        });
    }

    void save(){
        String editedDescription = description.getText().toString();

        Intent data = new Intent();
        data.putExtra(MainActivity.ID, eventId);
        data.putExtra(MainActivity.START_TIME, startTimeCalendar);
        data.putExtra(MainActivity.END_TIME, endTimeCalendar);
        data.putExtra(MainActivity.DESCRIPTION, editedDescription);
        setResult(ACTION_EDIT, data);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_delete:
                createDeletionDiaolg();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void createDeletionDiaolg(){
        new MaterialDialog.Builder(EventInfoActivity.this)
                .title("Ви впевнені")
                .positiveText("Так")
                .negativeText("Ні")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent data = new Intent();
                        data.putExtra(EventInfoActivity.ID, eventId);
                        setResult(ACTION_DELETE, data);
                        finish();
                    }
                })
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_event, menu);
        return true;
    }
}
