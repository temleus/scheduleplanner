package kpi.com.kpishedule.data;

import java.util.ArrayList;
import java.util.List;

public class Day {

    private List<LessonEntry> lessons = new ArrayList<LessonEntry>();

    public void addLesson(LessonEntry lesson, int number){
        lessons.add(number, lesson);
    }

    public List<LessonEntry> getLessons() {
        return lessons;
    }
}
