package kpi.com.kpishedule.data;

import java.util.ArrayList;
import java.util.List;

public class Week {
    public List<Day> days;

    public Week() {
        this.days = new ArrayList<Day>(7);
        for(int i = 0; i < 6; i++ ){
            days.add(new Day());
        }
    }

    public List<Day> getDays() {
        return days;
    }
}
