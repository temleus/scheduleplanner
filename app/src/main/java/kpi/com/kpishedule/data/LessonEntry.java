package kpi.com.kpishedule.data;

public class LessonEntry {

    private int numberInWeek;
    private String thisWeekLesson, nextWeekLesson;

//    private boolean lessonSplit;

    public LessonEntry(int numberInWeek) {
        this.numberInWeek = numberInWeek;
//        lessonSplit = false;
    }

    public LessonEntry(int numberInWeek, String thisWeekLesson) {
        this.numberInWeek = numberInWeek;
        this.thisWeekLesson = thisWeekLesson;
//        lessonSplit = false;
    }

    public LessonEntry(int numberInWeek, String thisWeekLesson, String nextWeekLesson) {
        this.numberInWeek = numberInWeek;
        this.thisWeekLesson = thisWeekLesson;
        this.nextWeekLesson = nextWeekLesson;
//        lessonSplit = true;
    }

    public int getNumberInWeek() {
        return numberInWeek;
    }

    public String getThisWeekLesson() {
        return thisWeekLesson;
    }

    public String getNextWeekLesson() {
        return nextWeekLesson;
    }

    /** if true - there are no lesson*/
    public boolean isEmpty() {
        return thisWeekLesson == null;
    }

    /** if true - there are two different lessons depends on week*/
    public boolean isLessonSplit() {
        return (thisWeekLesson != null && !thisWeekLesson.equals(nextWeekLesson))
                || (nextWeekLesson != null && !nextWeekLesson.equals(thisWeekLesson));
    }
}
