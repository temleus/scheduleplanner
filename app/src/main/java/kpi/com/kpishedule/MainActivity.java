package kpi.com.kpishedule;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.RectF;
import android.nfc.Tag;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;

import com.alamkanak.weekview.WeekViewEvent;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class MainActivity extends BaseActivity {

    public static final String START_TIME = "startTime";
    public static final String END_TIME = "endTime";
    public static final String DESCRIPTION = "description";
    public static final String ID = "id";

    private static final int ADD_EVENT_ACTIVITY = 0;
    private static final int INFO_EVENT_ACTIVITY = 1;

    List<WeekViewEvent> events = new ArrayList<>();
    static Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        if(sp.contains("events")){
            Type listType = new TypeToken<List<WeekViewEvent>>() {}.getType();
            List<WeekViewEvent> list = restore(sp, "events", listType);
            events.addAll(list);
        }
    }

    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        List<WeekViewEvent> thisMonthEvents = new ArrayList<>();
        for(WeekViewEvent event: events){
            if(eventMatches(event, newYear, newMonth)){
                thisMonthEvents.add(event);
            }
        }
        return thisMonthEvents;
    }


    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        startActivityForResult(new Intent(this, EventInfoActivity.class)
                .putExtra(EventInfoActivity.DESCRIPTION, event.getName())
                .putExtra(EventInfoActivity.ID, event.getId())
                .putExtra(EventInfoActivity.START_TIME, format.format(event.getStartTime().getTime()))
                .putExtra(EventInfoActivity.END_TIME, format.format(event.getEndTime().getTime()))
                , INFO_EVENT_ACTIVITY, null);
    }

    private boolean eventMatches(WeekViewEvent event, int year, int month) {
        return (event.getStartTime().get(Calendar.YEAR) == year && event.getStartTime().get(Calendar.MONTH) == month - 1) || (event.getEndTime().get(Calendar.YEAR) == year && event.getEndTime().get(Calendar.MONTH) == month - 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ADD_EVENT_ACTIVITY){
            Calendar startTime = (Calendar) data.getExtras().getSerializable(START_TIME);
            Calendar endTime = (Calendar) data.getExtras().getSerializable(END_TIME);
            String description = data.getExtras().getString(DESCRIPTION);
            if (resultCode == AddEventActivity.RESULT_OK) {
                WeekViewEvent event = new WeekViewEvent(1, description, startTime, endTime);
                event.setColor(rndColor());
                if(!events.isEmpty()){
                    event.setId(events.get(events.size() -1).getId() + 1);
                }
                events.add(event);
            }
            getWeekView().notifyDatasetChanged();
        } else if (requestCode == INFO_EVENT_ACTIVITY){
             if (resultCode == EventInfoActivity.ACTION_EDIT){
                 Calendar startTime = (Calendar) data.getExtras().getSerializable(START_TIME);
                 Calendar endTime = (Calendar) data.getExtras().getSerializable(END_TIME);
                 String description = data.getExtras().getString(DESCRIPTION);
                long id = data.getExtras().getLong(ID);
                WeekViewEvent eventEdited = null;
                for(WeekViewEvent event: events){
                    if(event.getId() == id) eventEdited = event;
                }
                eventEdited.setName(description);
                eventEdited.setStartTime(startTime);
                eventEdited.setEndTime(endTime);
                 getWeekView().notifyDatasetChanged();
            } else if (resultCode == EventInfoActivity.ACTION_DELETE) {
                long id = data.getExtras().getLong(ID);
                for(WeekViewEvent weekViewEvent: events){
                    if(weekViewEvent.getId() == id){
                        events.remove(weekViewEvent);
                    }
                }
                 getWeekView().notifyDatasetChanged();
            }
        }
    }

    Random random = new Random();
    int rndColor(){
        int randomNumber = random.nextInt();
        return Color.parseColor("#" + String.valueOf(randomNumber).substring(0, 6));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_add:
                startActivityForResult(new Intent(this, AddEventActivity.class), ADD_EVENT_ACTIVITY, null);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(!events.isEmpty()){
            save(PreferenceManager.getDefaultSharedPreferences(this), events, "events");
        }
    }

    /** methods for storing arbitrary objects at shared preferences */
    public static <T> void save(SharedPreferences sharedPreferences, T object, String key) {
        String json = gson.toJson(object, object.getClass());
        sharedPreferences.edit().putString(key, json).commit();
    }

    public static <T> T restore(SharedPreferences sharedPreferences, String key, Type klass) {
        if (sharedPreferences.contains(key)) {
            String json = sharedPreferences.getString(key, "");
            T object = gson.fromJson(json, klass);
            return object;
        } else {
            return null;
        }
    }
}
