package kpi.com.kpishedule;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Constants {
    public  static DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
}
